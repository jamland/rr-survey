angular.module('rrApp.models.steps', [])
  .service('StepsModel', function ($http) {
    var model = this,
      URLS = {
        FETCH: 'scripts/src/models/survey.json'
      };

    model.getSteps = function () {
      return $http.get(URLS.FETCH);
    }
  })
;
