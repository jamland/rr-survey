angular.module('rrApp', [
  'ui.router',
  'ngAnimate',
  'rrApp.models.steps',
  'countrySelect',
  'ui.mask'
])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('wizard', {
        url: '/wizard',
        templateUrl: 'scripts/src/templates/wizard.tmpl.html',
        controller: 'wizardCtrl'
      })
      .state('wizard.steps', {
        url: '/{stepId}',
        views: {
          '': {
            templateUrl: 'scripts/src/templates/wizard-step.tmpl.html',
            controller: 'StepsCtrl as stepsCtrl'
          }
        }
      })
    ;
    $urlRouterProvider.otherwise('/wizard/1');
  })
  .controller('wizardCtrl', function ($scope, stagesService) {
    $scope.stages = stagesService.getData();
    $scope.currentStage = stagesService.getCurrent();
    $scope.surveyData = {};
    $scope.formData = {};
    console.log('wizardCtrl init');
  })
  .controller('StepsCtrl', function ($scope, $state, stagesService, $stateParams) {
    var stepsCtrl = this;
    stepsCtrl.formData = {};

    $scope.stages = stagesService.getData();

    if ( $stateParams.stepId=='finish' ) {
      $scope.currentStage = $stateParams.stepId;
    }
    else {
      $scope.currentStage = $stateParams.stepId-1 || 0;
    }
    stagesService.setCurrent($scope.currentStage);

    function nextStep(stepData) {
      console.log('next step -->');
      $scope.currentStage += 1;
      stagesService.setCurrent($scope.currentStage);

      if ($scope.currentStage == $scope.stages.length) {
        $state.go("wizard.steps", {'stepId': 'finish'});
      }
      else {
        $state.go("wizard.steps", {'stepId': $scope.currentStage+1});
      }
    }

    $scope.nextStep = nextStep;
  })
  .service('stagesService', function (StepsModel) {
    var service = this;
    service.stages = {};
    service.currentStage = 0;

    StepsModel.getSteps()
      .then(function (result) {
        service.stages = result.data;
      });

    service.getData = function () {
      return service.stages;
    }

    service.setData = function (stages) {
      service.stages = stages;
      return true;
    }

    service.getCurrent = function () {
      return service.currentStage;
    }

    service.setCurrent = function (step) {
      service.currentStage = step;
      return true;
    }
  })
  .directive('checkZip', function() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, elem, attr, ngModel) {
        if(attr.name=="zip") {
          ngModel.$validators.zipcode = function(val) {
            var regexp = /^\d{5}(?:[-\s]\d{4})?$/;
            if (val) {
              return regexp.test(val);
            } else {
              return true;
            }
          };
        }
      }
    };
  })
;
