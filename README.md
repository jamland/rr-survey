RR Survey
==========================

## Dev Process

I have used html5/SASS/Gulp boilerplate for init project `https://github.com/ryanbenson/Harvest`

You can find source files in `app/`:

- JS in `app/scripts/src`
- SASS in `app/styles/scss`
- HTML tamplates in `app/scripts/src/templates`
- Data in `app/scripts/src/models`

You can start app using:

```
npm install
gulp
```

You can deploy `app/` source files with:

```
gulp deploy
```

## Issues

- input: Select with search, Country name

Done with select without search

- input: String, Valid zip code	

Directive for validation doesn't work properly in some case


